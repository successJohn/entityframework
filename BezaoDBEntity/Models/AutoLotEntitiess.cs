using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace BezaoEntity.Models
{
    public partial class AutoLotEntitiess : DbContext
    {
        public AutoLotEntitiess()
            : base("name=AutoLotConnection")
        {
        }

        public virtual DbSet<DBAccount> Accounts { get; set; }
        public virtual DbSet<DBTransaction> Transactions { get; set; }
        public virtual DbSet<DBUser> Users { get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DBAccount>()
                .Property(e => e.Balance)
                .HasPrecision(38, 2);

            modelBuilder.Entity<DBTransaction>()
                .Property(e => e.Amount)
                .HasPrecision(38, 2);
        }
    }
}
